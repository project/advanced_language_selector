<?php

namespace Drupal\advanced_language_selector;

/**
 * Lang code utilities.
 */
class Langcodes {

  /**
   * Try to convert langcodes to each corresponding country code flag.
   *
   * @param string $langcode
   *   The lang code.
   *
   * @return string
   *   The country code (flag code).
   */
  public static function langcodeToCountryCode(string $langcode): string {
    switch ($langcode) {
      case 'ast': // Asturian
        return 'es-ast';
      case 'es-sv': // El Salvador
        return 'sv';
      case 'gl': // España (Galicia)
        return 'es-gl';
      case 'eu': // España (País Vasco)
        return 'es-eu';
      case 'ca': // España (Cataluña)
        return 'es-ca';
      case (preg_match('/^es(-.*)?$/', $langcode) ? TRUE : FALSE): // Spain
        return 'es';
      case (preg_match('/^af(-.*)?$/', $langcode) ? TRUE : FALSE): // Sudáfrica
        return 'za';
      case (preg_match('/^sq(-.*)?$/', $langcode) ? TRUE : FALSE): // Albania
        return 'al';
      case (preg_match('/^de(-.*)?$/', $langcode) ? TRUE : FALSE): // Alemania
        return 'de';
      case (preg_match('/^am(-.*)?$/', $langcode) ? TRUE : FALSE): // Etiopía
        return 'et';
      case (preg_match('/^ar(-.*)?$/', $langcode) ? TRUE : FALSE): // Arabia Saudita
        return 'sa';
      case (preg_match('/^hy(-.*)?$/', $langcode) ? TRUE : FALSE): // Armenia
        return 'am';
      case (preg_match('/^az(-.*)?$/', $langcode) ? TRUE : FALSE): // Azerbaiyán
        return 'az';
      case (preg_match('/^ms(-.*)?$/', $langcode) ? TRUE : FALSE): // Malasia
        return 'my';
      case (preg_match('/^be(-.*)?$/', $langcode) ? TRUE : FALSE): // Bielorrusia
        return 'by';
      case (preg_match('/^bn(-.*)?$/', $langcode) ? TRUE : FALSE): // Bangladesh
        return 'bd';
      case (preg_match('/^bs(-.*)?$/', $langcode) ? TRUE : FALSE): // Bosnia y Herzegovina
        return 'ba';
      case (preg_match('/^br(-.*)?$/', $langcode) ? TRUE : FALSE): // Francia (Bretaña)
        return 'fr';
      case (preg_match('/^bg(-.*)?$/', $langcode) ? TRUE : FALSE): // Bulgaria
        return 'bg';
      case (preg_match('/^my(-.*)?$/', $langcode) ? TRUE : FALSE): // Myanmar
        return 'mm';
      case 'zh-hans': // China (Simplificado)
        return 'cn';
      case 'zh-hant': // Taiwán (Tradicional)
        return 'tw';
      case (preg_match('/^hr(-.*)?$/', $langcode) ? TRUE : FALSE): // Croacia
        return 'hr';
      case (preg_match('/^cs(-.*)?$/', $langcode) ? TRUE : FALSE): // República Checa
        return 'cz';
      case (preg_match('/^da(-.*)?$/', $langcode) ? TRUE : FALSE): // Dinamarca
        return 'dk';
      case (preg_match('/^nl(-.*)?$/', $langcode) ? TRUE : FALSE): // Países Bajos
        return 'nl';
      case (preg_match('/^dz(-.*)?$/', $langcode) ? TRUE : FALSE): // Bután
        return 'bt';
      case (preg_match('/^en(-.*)?$/', $langcode) ? TRUE : FALSE): // Reino Unido
        return 'gb';
      case (preg_match('/^et(-.*)?$/', $langcode) ? TRUE : FALSE): // Estonia
        return 'ee';
      case (preg_match('/^fo(-.*)?$/', $langcode) ? TRUE : FALSE): // Islas Feroe
        return 'fo';
      case (preg_match('/^fil(-.*)?$/', $langcode) ? TRUE : FALSE): // Filipinas
        return 'ph';
      case (preg_match('/^fi(-.*)?$/', $langcode) ? TRUE : FALSE): // Finlandia
        return 'fi';
      case (preg_match('/^fr(-.*)?$/', $langcode) ? TRUE : FALSE): // Francia
        return 'fr';
      case (preg_match('/^fy(-.*)?$/', $langcode) ? TRUE : FALSE): // Países Bajos (Frisia)
        return 'nl';
      case (preg_match('/^ka(-.*)?$/', $langcode) ? TRUE : FALSE): // Georgia
        return 'ge';
      case (preg_match('/^el(-.*)?$/', $langcode) ? TRUE : FALSE): // Grecia
        return 'gr';
      case (preg_match('/^gu(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Gujarat)
        return 'in';
      case (preg_match('/^ht(-.*)?$/', $langcode) ? TRUE : FALSE): // Haití
        return 'ht';
      case (preg_match('/^haw(-.*)?$/', $langcode) ? TRUE : FALSE): // Estados Unidos (Hawái)
        return 'us';
      case (preg_match('/^he(-.*)?$/', $langcode) ? TRUE : FALSE): // Israel
        return 'il';
      case (preg_match('/^hi(-.*)?$/', $langcode) ? TRUE : FALSE): // India
        return 'in';
      case (preg_match('/^hu(-.*)?$/', $langcode) ? TRUE : FALSE): // Hungría
        return 'hu';
      case (preg_match('/^is(-.*)?$/', $langcode) ? TRUE : FALSE): // Islandia
        return 'is';
      case (preg_match('/^id(-.*)?$/', $langcode) ? TRUE : FALSE): // Indonesia
        return 'id';
      case (preg_match('/^ga(-.*)?$/', $langcode) ? TRUE : FALSE): // Irlanda
        return 'ie';
      case (preg_match('/^it(-.*)?$/', $langcode) ? TRUE : FALSE): // Italia
        return 'it';
      case (preg_match('/^ja(-.*)?$/', $langcode) ? TRUE : FALSE): // Japón
        return 'jp';
      case (preg_match('/^jv(-.*)?$/', $langcode) ? TRUE : FALSE): // Indonesia
        return 'id';
      case (preg_match('/^kn(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Karnataka)
        return 'in';
      case (preg_match('/^kk(-.*)?$/', $langcode) ? TRUE : FALSE): // Kazajistán
        return 'kz';
      case (preg_match('/^km(-.*)?$/', $langcode) ? TRUE : FALSE): // Camboya
        return 'kh';
      case (preg_match('/^rw(-.*)?$/', $langcode) ? TRUE : FALSE): // Ruanda
        return 'rw';
      case (preg_match('/^ko(-.*)?$/', $langcode) ? TRUE : FALSE): // Corea del Sur
        return 'kr';
      case (preg_match('/^ku(-.*)?$/', $langcode) ? TRUE : FALSE): // Irak (Kurdistán)
        return 'iq';
      case (preg_match('/^ky(-.*)?$/', $langcode) ? TRUE : FALSE): // Kirguistán
        return 'kg';
      case (preg_match('/^lo(-.*)?$/', $langcode) ? TRUE : FALSE): // Laos
        return 'la';
      case (preg_match('/^lv(-.*)?$/', $langcode) ? TRUE : FALSE): // Letonia
        return 'lv';
      case (preg_match('/^lt(-.*)?$/', $langcode) ? TRUE : FALSE): // Lituania
        return 'lt';
      case (preg_match('/^mk(-.*)?$/', $langcode) ? TRUE : FALSE): // Macedonia del Norte
        return 'mk';
      case (preg_match('/^mg(-.*)?$/', $langcode) ? TRUE : FALSE): // Madagascar
        return 'mg';
      case (preg_match('/^ml(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Kerala)
        return 'in';
      case (preg_match('/^mt(-.*)?$/', $langcode) ? TRUE : FALSE): // Malta
        return 'mt';
      case (preg_match('/^mr(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Maharashtra)
        return 'in';
      case (preg_match('/^mn(-.*)?$/', $langcode) ? TRUE : FALSE): // Mongolia
        return 'mn';
      case (preg_match('/^ne(-.*)?$/', $langcode) ? TRUE : FALSE): // Nepal
        return 'np';
      case (preg_match('/^sv(-.*)?$/', $langcode) ? TRUE : FALSE): // Swedish
        return 'se';
      case (preg_match('/^se(-.*)?$/', $langcode) ? TRUE : FALSE): // Suecia (Sami)
        return 'se';
      case (preg_match('/^nb(-.*)?$/', $langcode) ? TRUE : FALSE): // Noruega (Bokmål)
        return 'no';
      case (preg_match('/^nn(-.*)?$/', $langcode) ? TRUE : FALSE): // Noruega (Nynorsk)
        return 'no';
      case (preg_match('/^oc(-.*)?$/', $langcode) ? TRUE : FALSE): // Francia (Occitania)
        return 'fr';
      case (preg_match('/^or(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Odisha)
        return 'in';
      case (preg_match('/^os(-.*)?$/', $langcode) ? TRUE : FALSE): // Rusia (Osetia del Norte)
        return 'ru';
      case (preg_match('/^ps(-.*)?$/', $langcode) ? TRUE : FALSE): // Afganistán
        return 'af';
      case (preg_match('/^prs(-.*)?$/', $langcode) ? TRUE : FALSE): // Afganistán
        return 'af';
      case (preg_match('/^fa(-.*)?$/', $langcode) ? TRUE : FALSE): // Irán
        return 'ir';
      case (preg_match('/^pl(-.*)?$/', $langcode) ? TRUE : FALSE): // Polonia
        return 'pl';
      case 'pt-br': // Brasil
        return 'br';
      case (preg_match('/^pt(-.*)?$/', $langcode) ? TRUE : FALSE): // Portugal
        return 'pt';
      case (preg_match('/^pa(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Punjab)
        return 'in';
      case (preg_match('/^ro(-.*)?$/', $langcode) ? TRUE : FALSE): // Rumania
        return 'ro';
      case (preg_match('/^ru(-.*)?$/', $langcode) ? TRUE : FALSE): // Rusia
        return 'ru';
      case (preg_match('/^sr(-.*)?$/', $langcode) ? TRUE : FALSE): // Serbia
        return 'rs';
      case (preg_match('/^si(-.*)?$/', $langcode) ? TRUE : FALSE): // Sri Lanka
        return 'lk';
      case (preg_match('/^sk(-.*)?$/', $langcode) ? TRUE : FALSE): // Eslovaquia
        return 'sk';
      case (preg_match('/^sl(-.*)?$/', $langcode) ? TRUE : FALSE): // Eslovenia
        return 'si';
      case (preg_match('/^sw(-.*)?$/', $langcode) ? TRUE : FALSE): // Kenia
        return 'ke';
      case (preg_match('/^ta(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Tamil Nadu)
        return 'in';
      case (preg_match('/^te(-.*)?$/', $langcode) ? TRUE : FALSE): // India (Andhra Pradesh)
        return 'in';
      case (preg_match('/^th(-.*)?$/', $langcode) ? TRUE : FALSE): // Tailandia
        return 'th';
      case (preg_match('/^tr(-.*)?$/', $langcode) ? TRUE : FALSE): // Turquía
        return 'tr';
      case (preg_match('/^tk(-.*)?$/', $langcode) ? TRUE : FALSE): // Turkmenistán
        return 'tm';
      case (preg_match('/^uk(-.*)?$/', $langcode) ? TRUE : FALSE): // Ucrania
        return 'ua';
      case (preg_match('/^ur(-.*)?$/', $langcode) ? TRUE : FALSE): // Pakistán
        return 'pk';
      case (preg_match('/^uz(-.*)?$/', $langcode) ? TRUE : FALSE): // Uzbekistán
        return 'uz';
      case (preg_match('/^vi(-.*)?$/', $langcode) ? TRUE : FALSE): // Vietnam
        return 'vn';
      case (preg_match('/^cy(-.*)?$/', $langcode) ? TRUE : FALSE): // Gales
        return 'gb';
      case (preg_match('/^xh(-.*)?$/', $langcode) ? TRUE : FALSE): // Sudáfrica (Xhosa)
        return 'za';
      case (preg_match('/^zu(-.*)?$/', $langcode) ? TRUE : FALSE): // Sudáfrica (Zulú)
        return 'za';
      case 'eo': // Esperanto
      default:
        return 'no-flag';
    }
  }
}

